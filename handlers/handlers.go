package handlers

import (
	"encoding/json"
	"net/http"
	"ozon_books/service"
	"strconv"
)

type Controller struct {
	ls service.LibrarianServicer
}

func NewController(ls service.LibrarianServicer) *Controller {
	return &Controller{ls: ls}
}

func (c *Controller) UserCreate(w http.ResponseWriter, r *http.Request) {
	username := r.FormValue("username")
	mail := r.FormValue("mail")

	if username == "" || mail == "" {
		http.Error(w, "Missing required parameters", http.StatusBadRequest)
	}

	err := c.ls.CreateUser(username, mail)
	if err != nil {
		http.Error(w, "Failed to create user", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	_, err = w.Write([]byte("User created successfully"))
	if err != nil {
		http.Error(w, "Falied to write successful data", http.StatusBadRequest)
		return
	}

}

func (c *Controller) AuthorCreate(w http.ResponseWriter, r *http.Request) {
	firstname := r.FormValue("firstname")
	lastname := r.FormValue("lastname")

	if firstname == "" || lastname == "" {
		http.Error(w, "Missing required parameters", http.StatusBadRequest)
		return
	}

	err := c.ls.CreateAuthor(firstname, lastname)
	if err != nil {
		// Обработка ошибки, если создание автора не удалось
		http.Error(w, "Failed to create author", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	_, err = w.Write([]byte("Author created successfully"))
	if err != nil {
		http.Error(w, "Failed to send successful data", http.StatusBadRequest)
	}

}

func (c *Controller) CreateBook(w http.ResponseWriter, r *http.Request) {
	title := r.FormValue("title")
	authorID := r.FormValue("authorID")
	userID := r.FormValue("userID")

	if title == "" || authorID == "" || userID == "" {
		http.Error(w, "Missing required parameters", http.StatusBadRequest)
		return
	}

	authorIDInt, err := strconv.Atoi(authorID)
	if err != nil {
		http.Error(w, "Invalid authorID", http.StatusBadRequest)
		return
	}

	userIDInt, err := strconv.Atoi(userID)
	if err != nil {
		http.Error(w, "Invalid userID", http.StatusBadRequest)
		return
	}

	err = c.ls.CreateBook(title, authorIDInt, userIDInt)
	if err != nil {
		http.Error(w, "Failed to create book", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	_, err = w.Write([]byte("Book created successfully"))
	if err != nil {
		http.Error(w, "Failed to send successful data", http.StatusBadRequest)
	}
}

func (c *Controller) GetUserList(w http.ResponseWriter, r *http.Request) {
	users, err := c.ls.GetUserList()
	if err != nil {
		http.Error(w, "Failed to get user list", http.StatusInternalServerError)
		return
	}

	usersJSON, err := json.Marshal(users)
	if err != nil {
		http.Error(w, "Failed to convert user list to JSON", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(usersJSON)
	if err != nil {
		http.Error(w, "Can't write users", http.StatusBadRequest)
	}
}

func (c *Controller) GetAuthorList(w http.ResponseWriter, r *http.Request) {
	authors, err := c.ls.GetAuthorList()
	if err != nil {
		http.Error(w, "Failed to get author list", http.StatusInternalServerError)
		return
	}

	authorsJSON, err := json.Marshal(authors)
	if err != nil {
		http.Error(w, "Failed to convert author list to JSON", http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")

	w.WriteHeader(http.StatusOK)
	_, err = w.Write(authorsJSON)
	if err != nil {
		http.Error(w, "Can't write authors", http.StatusBadRequest)
	}

}

func (c *Controller) GetAllBooks(w http.ResponseWriter, r *http.Request) {
	books, err := c.ls.GetAllBooks()
	if err != nil {
		http.Error(w, "Failed to get book list", http.StatusInternalServerError)
		return
	}

	booksJSON, err := json.Marshal(books)
	if err != nil {
		http.Error(w, "Failed to convert book list to JSON", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	w.WriteHeader(http.StatusOK)
	_, err = w.Write(booksJSON)
	if err != nil {
		http.Error(w, "Can't write books", http.StatusBadRequest)
	}
}

func (c *Controller) GetBookByID(w http.ResponseWriter, r *http.Request) {
	bookIDParam := r.URL.Query().Get("id")
	bookID, err := strconv.Atoi(bookIDParam)
	if err != nil {
		http.Error(w, "Invalid book ID", http.StatusBadRequest)
		return
	}

	book, err := c.ls.GetBookByID(bookID)
	if err != nil {
		http.Error(w, "Failed to get book details", http.StatusInternalServerError)
		return
	}

	bookJSON, err := json.Marshal(book)
	if err != nil {
		http.Error(w, "Failed to convert book details to JSON", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	w.WriteHeader(http.StatusOK)
	_, err = w.Write(bookJSON)
	if err != nil {
		http.Error(w, "Can't write book", http.StatusBadRequest)
	}
}

func (c *Controller) BorrowBook(w http.ResponseWriter, r *http.Request) {
	bookIDStr := r.FormValue("book_id")
	userIDStr := r.FormValue("user_id")

	if bookIDStr == "" || userIDStr == "" {
		http.Error(w, "Missing required parameters", http.StatusBadRequest)
		return
	}

	bookID, err := strconv.Atoi(bookIDStr)
	if err != nil {
		http.Error(w, "Invalid book_id", http.StatusBadRequest)
		return
	}

	userID, err := strconv.Atoi(userIDStr)
	if err != nil {
		http.Error(w, "Invalid user_id", http.StatusBadRequest)
		return
	}

	err = c.ls.BorrowBook(bookID, userID)
	if err != nil {
		http.Error(w, "Failed to borrow book", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (c *Controller) ReturnBook(w http.ResponseWriter, r *http.Request) {
	bookIDStr := r.FormValue("book_id")

	if bookIDStr == "" {
		http.Error(w, "Missing required parameter", http.StatusBadRequest)
		return
	}

	bookID, err := strconv.Atoi(bookIDStr)
	if err != nil {
		http.Error(w, "Invalid book_id", http.StatusBadRequest)
		return
	}

	err = c.ls.ReturnBook(bookID)
	if err != nil {
		http.Error(w, "Failed to return book", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}
