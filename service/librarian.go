package service

import (
	"fmt"
	"ozon_books/models"
	"ozon_books/repo"
)

type LibrarianService struct {
	authorRepo repo.AuthorStorager
	bookRepo   repo.BookStorager
	userRepo   repo.UserStorager
}

type LibrarianServicer interface {
	CreateAuthor(firstname, lastname string) error
	CreateUser(username, mail string) error
	CreateBook(title string, authorID, userID int) error
	GetUserList() ([]models.UserWithBooks, error)
	GetAuthorList() ([]models.AuthorWithBooks, error)
	GetAllBooks() ([]models.BookWithAuthor, error)
	GetBookByID(id int) (models.BookWithAuthor, error)
	BorrowBook(bookID, userID int) error
	ReturnBook(bookID int) error
}

func NewLibrarianService(authorRepo repo.AuthorStorager, bookRepo repo.BookStorager, userRepo repo.UserStorager) *LibrarianService {
	return &LibrarianService{
		authorRepo: authorRepo,
		bookRepo:   bookRepo,
		userRepo:   userRepo,
	}
}

func (ls *LibrarianService) CreateAuthor(firstname, lastname string) error {
	if err := ls.authorRepo.CreateAuthor(firstname, lastname); err != nil {
		return fmt.Errorf("failed to create author: %s", err.Error())
	}

	return nil
}

func (ls *LibrarianService) CreateUser(username, mail string) error {
	if err := ls.userRepo.CreateUser(username, mail); err != nil {
		return fmt.Errorf("failed to create author: %s", err.Error())
	}

	return nil
}

func (ls *LibrarianService) CreateBook(title string, authorID, userID int) error {
	if _, err := ls.authorRepo.GetAuthorByID(authorID); err != nil {
		return fmt.Errorf("author not found: %s", err.Error())
	}

	if err := ls.bookRepo.CreateBook(title, authorID, userID); err != nil {
		return fmt.Errorf("failed to create book: %s", err.Error())
	}

	return nil
}

func (ls *LibrarianService) GetUserList() ([]models.UserWithBooks, error) {
	users, err := ls.userRepo.GetAllUsers()
	if err != nil {
		return nil, fmt.Errorf("can't get all users: %s", err.Error())
	}

	usersWithBooks := make([]models.UserWithBooks, 0, len(users))

	for _, user := range users {
		RentedBooks, err := ls.bookRepo.GetBooksByUserID(user.ID)
		if err != nil {
			return nil, fmt.Errorf("can't get books by user_id: %s", err.Error())
		}
		usersWithBooks = append(usersWithBooks, models.UserWithBooks{User: user, RentedBooks: RentedBooks})
	}

	return usersWithBooks, nil
}

func (ls *LibrarianService) GetAuthorList() ([]models.AuthorWithBooks, error) {
	authors, err := ls.authorRepo.GetAllAuthors()
	if err != nil {
		return nil, fmt.Errorf("can't get all authors: %s", err.Error())
	}

	authorsWithBooks := make([]models.AuthorWithBooks, 0, len(authors))

	for _, author := range authors {
		Books, err := ls.bookRepo.GetBooksByAuthorID(author.ID)
		if err != nil {
			return nil, fmt.Errorf("can't get books by author_id: %s", err.Error())
		}
		authorsWithBooks = append(authorsWithBooks, models.AuthorWithBooks{Author: author, Books: Books})
	}

	return authorsWithBooks, nil
}

func (ls *LibrarianService) GetAllBooks() ([]models.BookWithAuthor, error) {
	books, err := ls.bookRepo.GetAllBooks()
	if err != nil {
		return nil, fmt.Errorf("failed to get all books %s", err.Error())
	}

	var booksWithAuthor []models.BookWithAuthor

	for _, book := range books {
		author, err := ls.authorRepo.GetAuthorByID(book.Author_ID)
		if err != nil {
			return nil, fmt.Errorf("failed to get author for book (ID: %d): %s", book.ID, err.Error())
		}
		bookWithAuthor := models.BookWithAuthor{
			Book:   book,
			Author: author,
		}
		booksWithAuthor = append(booksWithAuthor, bookWithAuthor)
	}

	return booksWithAuthor, nil
}

func (ls *LibrarianService) GetBookByID(id int) (models.BookWithAuthor, error) {
	book, err := ls.bookRepo.GetBookByID(id)
	if err != nil {
		return models.BookWithAuthor{}, fmt.Errorf("failed to get book: %s", err.Error())
	}

	if book.User_ID != 0 {
		return models.BookWithAuthor{}, fmt.Errorf("book is already borrowed by user (ID: %d)", book.User_ID)
	}

	author, err := ls.authorRepo.GetAuthorByID(book.Author_ID)
	if err != nil {
		return models.BookWithAuthor{}, fmt.Errorf("failed to get author for book (ID: %d): %s", book.ID, err.Error())
	}

	bookWithAuthor := models.BookWithAuthor{
		Book:   book,
		Author: author,
	}

	return bookWithAuthor, nil
}

func (ls *LibrarianService) BorrowBook(bookID, userID int) error {
	book, err := ls.bookRepo.GetBookByID(bookID)
	if err != nil {
		return fmt.Errorf("failed to get book: %s", err.Error())
	}

	if book.User_ID != 0 {
		return fmt.Errorf("book is already borrowed by user (ID: %d)", book.User_ID)
	}

	err = ls.bookRepo.UpdateBookUser(bookID, userID)
	if err != nil {
		return fmt.Errorf("failed to update book user: %s", err.Error())
	}

	return nil
}

func (ls *LibrarianService) ReturnBook(bookID int) error {
	book, err := ls.bookRepo.GetBookByID(bookID)
	if err != nil {
		return fmt.Errorf("failed to get book: %s", err.Error())
	}

	if book.User_ID == 0 {
		return fmt.Errorf("book is already returned")
	}

	err = ls.bookRepo.ReturnBook(bookID)
	if err != nil {
		return fmt.Errorf("failed to return book: %s", err.Error())
	}

	return nil
}
