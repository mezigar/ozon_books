package models

type User struct {
	ID       int    `json:"id" db:"id"`
	Username string `json:"username" db:"username"`
	Mail     string `json:"mail" db:"mail"`
}

type UserWithBooks struct {
	User
	RentedBooks []Book
}
