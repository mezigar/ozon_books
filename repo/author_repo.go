package repo

import (
	"context"
	"fmt"
	"ozon_books/models"
	"sync"

	"github.com/jackc/pgx/v5"
)

type AuthorStorage struct {
	db                 *pgx.Conn
	autoIncrementCount int
	*sync.Mutex
}

type AuthorStorager interface {
	CreateAuthor(firstname, lastname string) error
	GetAuthorByID(id int) (models.Author, error)
	GetAllAuthors() ([]models.Author, error)
}

func NewAuthorStorage(db *pgx.Conn) *AuthorStorage {
	return &AuthorStorage{
		db:                 db,
		autoIncrementCount: 1,
		Mutex:              &sync.Mutex{},
	}
}

func (a *AuthorStorage) CreateAuthor(firstname, lastname string) error {
	a.Mutex.Lock()
	defer a.Mutex.Unlock()
	_, err := a.db.Exec(context.Background(),
		"INSERT INTO authors (id, firstname, lastname) VALUES ($1, $2, $3)",
		a.autoIncrementCount, firstname, lastname)
	if err != nil {
		fmt.Println(err)
		return err
	}
	a.autoIncrementCount++
	return nil
}

func (a *AuthorStorage) GetAuthorByID(id int) (models.Author, error) {
	a.Mutex.Lock()
	defer a.Mutex.Unlock()
	var author models.Author
	row := a.db.QueryRow(context.Background(),
		"SELECT id, firstname, lastname FROM authors WHERE id = $1",
		id)

	if err := row.Scan(&author.ID, &author.FirstName, &author.LastName); err != nil {
		return author, fmt.Errorf("can't scan author: %s", err.Error())
	}
	return author, nil
}

func (a *AuthorStorage) GetAllAuthors() ([]models.Author, error) {
	a.Mutex.Lock()
	defer a.Mutex.Unlock()

	rows, err := a.db.Query(context.Background(),
		"SELECT id, firstname, lastname FROM authors")

	if err != nil {
		return nil, fmt.Errorf("can't select all authors: %s", err.Error())
	}

	defer rows.Close()

	var result []models.Author

	for rows.Next() {
		author := models.Author{}
		if err := rows.Scan(&author.ID, &author.FirstName, &author.LastName); err != nil {
			return nil, fmt.Errorf("can't scan author: %s", err.Error())
		}
		result = append(result, author)
	}

	return result, nil
}

func (a *AuthorStorage) Close() {
	if a.db != nil {
		a.db.Close(context.Background())
	}
}
