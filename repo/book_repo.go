package repo

import (
	"context"
	"fmt"
	"ozon_books/models"
	"sync"

	"github.com/jackc/pgx/v5"
)

type BookStorage struct {
	db                 *pgx.Conn
	autoIncrementCount int
	*sync.Mutex
}

type BookStorager interface {
	CreateBook(title string, author_id, user_id int) error
	GetBookByTitle(title string) (models.Book, error)
	GetAllBooks() ([]models.Book, error)
	GetBooksByUserID(user_id int) ([]models.Book, error)
	GetBookByID(id int) (models.Book, error)
	GetBooksByAuthorID(author_id int) ([]models.Book, error)
	UpdateBookUser(id, userID int) error
	ReturnBook(id int) error
}

func NewBookStorage(db *pgx.Conn) *BookStorage {
	return &BookStorage{
		db:                 db,
		autoIncrementCount: 1,
		Mutex:              &sync.Mutex{},
	}
}

func (b *BookStorage) CreateBook(title string, author_id, user_id int) error {
	b.Mutex.Lock()
	defer b.Mutex.Unlock()

	// TODO: узнать, что автор по такому id существует
	// author, err := b.db.

	_, err := b.db.Exec(context.Background(),
		"INSERT INTO books (id, title, author_id, user_id) VALUES ($1, $2, $3, $4)",
		b.autoIncrementCount, title, author_id, user_id)
	if err != nil {
		fmt.Println(err)
		return err
	}
	b.autoIncrementCount++
	return nil
}

func (b *BookStorage) GetBookByTitle(title string) (models.Book, error) {
	b.Mutex.Lock()
	defer b.Mutex.Unlock()
	var book models.Book
	row := b.db.QueryRow(context.Background(),
		"SELECT id, title, author_id, user_id FROM books WHERE title = $1",
		title)

	if err := row.Scan(&book.ID, &book.Title, &book.Author_ID, &book.User_ID); err != nil {
		return book, fmt.Errorf("can't scan book: %s", err.Error())

	}
	return book, nil
}

func (b *BookStorage) GetAllBooks() ([]models.Book, error) {
	b.Mutex.Lock()
	defer b.Mutex.Unlock()

	rows, err := b.db.Query(context.Background(),
		"SELECT id, title, author_id, user_id FROM books")

	if err != nil {
		return nil, fmt.Errorf("can't select all books: %s", err.Error())
	}

	defer rows.Close()

	var result []models.Book
	for rows.Next() {
		book := models.Book{}
		if err := rows.Scan(&book.ID, &book.Title, &book.Author_ID, &book.User_ID); err != nil {
			return nil, fmt.Errorf("can't scan book: %s", err.Error())
		}

		result = append(result, book)
	}

	return result, nil
}

func (b *BookStorage) GetBooksByUserID(user_id int) ([]models.Book, error) {
	b.Mutex.Lock()
	defer b.Mutex.Unlock()

	rows, err := b.db.Query(context.Background(),
		"SELECT id, title, author_id, user_id FROM books WHERE user_id = $1", user_id)

	if err != nil {
		return nil, fmt.Errorf("can't select books by user_id: %s", err.Error())
	}

	var result []models.Book
	for rows.Next() {
		book := models.Book{}
		if err := rows.Scan(&book.ID, &book.Title, &book.Author_ID, &book.User_ID); err != nil {
			return nil, fmt.Errorf("can't scan book: %s", err.Error())
		}

		result = append(result, book)
	}

	return result, nil
}

func (b *BookStorage) GetBookByID(id int) (models.Book, error) {
	b.Mutex.Lock()
	defer b.Mutex.Unlock()

	var book models.Book
	row := b.db.QueryRow(context.Background(),
		"SELECT id, title, author_id, user_id FROM books WHERE id = $1",
		id)

	if err := row.Scan(&book.ID, &book.Title, &book.Author_ID, &book.User_ID); err != nil {
		return book, fmt.Errorf("can't scan book: %s", err.Error())
	}
	return book, nil
}

func (b *BookStorage) GetBooksByAuthorID(author_id int) ([]models.Book, error) {
	b.Mutex.Lock()
	defer b.Mutex.Unlock()

	rows, err := b.db.Query(context.Background(),
		"SELECT id, title, author_id, user_id FROM books WHERE author_id = $1", author_id)

	if err != nil {
		return nil, fmt.Errorf("can't select books by author_id: %s", err.Error())
	}

	var result []models.Book
	for rows.Next() {
		book := models.Book{}
		if err := rows.Scan(&book.ID, &book.Title, &book.Author_ID, &book.User_ID); err != nil {
			return nil, fmt.Errorf("can't scan book: %s", err.Error())
		}

		result = append(result, book)
	}

	return result, nil
}

func (b *BookStorage) UpdateBookUser(id, userID int) error {
	b.Mutex.Lock()
	defer b.Mutex.Unlock()

	_, err := b.db.Exec(context.Background(),
		"UPDATE books SET user_id = $1 WHERE id = $2",
		userID, id)
	if err != nil {
		return fmt.Errorf("failed to update book user: %s", err.Error())
	}
	return nil
}

func (b *BookStorage) ReturnBook(id int) error {
	b.Mutex.Lock()
	defer b.Mutex.Unlock()

	_, err := b.db.Exec(context.Background(),
		"UPDATE books SET user_id = 0 WHERE id = $1",
		id)
	if err != nil {
		return fmt.Errorf("failed to return book: %s", err.Error())
	}
	return nil
}
