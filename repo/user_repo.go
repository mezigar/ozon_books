package repo

import (
	"context"
	"fmt"
	"ozon_books/models"
	"sync"

	"github.com/jackc/pgx/v5"
)

type UserStorage struct {
	db                 *pgx.Conn
	autoIncrementCount int
	*sync.Mutex
}

type UserStorager interface {
	CreateUser(username, mail string) error
	GetUserByID(id int) (models.User, error)
	GetAllUsers() ([]models.User, error)
}

func NewUserStorage(db *pgx.Conn) *UserStorage {
	return &UserStorage{
		db:                 db,
		autoIncrementCount: 1,
		Mutex:              &sync.Mutex{},
	}
}

func (u *UserStorage) CreateUser(username, mail string) error {
	u.Mutex.Lock()
	defer u.Mutex.Unlock()
	_, err := u.db.Exec(context.Background(),
		"INSERT INTO users (id, username, mail) VALUES ($1, $2, $3)",
		u.autoIncrementCount, username, mail)
	if err != nil {
		fmt.Println(err)
		return err
	}
	u.autoIncrementCount++
	return nil
}

func (u *UserStorage) GetUserByID(id int) (models.User, error) {
	u.Mutex.Lock()
	defer u.Mutex.Unlock()
	var user models.User
	row := u.db.QueryRow(context.Background(),
		"SELECT id, username, mail FROM users WHERE id = $1",
		id)

	if err := row.Scan(&user.ID, &user.Username, &user.Mail); err != nil {
		return user, fmt.Errorf("can;t scan user: %s", err.Error())
	}
	return user, nil
}

func (u *UserStorage) GetAllUsers() ([]models.User, error) {
	u.Mutex.Lock()
	defer u.Mutex.Unlock()

	rows, err := u.db.Query(context.Background(),
		"SELECT id, username, mail FROM users")

	if err != nil {
		return nil, fmt.Errorf("can't select all users: %s", err.Error())
	}

	defer rows.Close()

	var result []models.User

	for rows.Next() {
		user := models.User{}
		if err := rows.Scan(&user.ID, &user.Username, &user.Mail); err != nil {
			return nil, fmt.Errorf("can't scan user: %s", err.Error())
		}
		result = append(result, user)
	}

	return result, nil
}
