package main

import "ozon_books/models"

func init() {
	_ = userAddRequest{}
	_ = authorAddRequest{}
	_ = bookAddRequest{}
	_ = borrowBookRequest{}
	_ = returnBookRequest{}
	_ = getAuthorRequest{}
	_ = getAuthorResponse{}
	_ = getBookByIDRequest{}
	_ = getBookByIDResponse{}
	_ = getBookRequest{}
	_ = getBookResponse{}
	_ = getUserRequest{}
	_ = getUserResponse{}
}

//go:generate swagger generate spec -o ./public/swagger.json --scan-models

// swagger:route POST /users user userAddRequest
// Добавление юзера
// responses:
//	200: description: Added successfuly.

// swagger:parameters userAddRequest
type userAddRequest struct {
	// Имя пользователя
	// required:true
	// in:formData
	Username string `json:"username"`

	// Почта пользователя
	// required:true
	// in:formData
	Mail string `json:"mail"`
}

// swagger:route POST /authors author authorAddRequest
// Добавление автора
// responses:
//	200: description: Added successfuly.

// swagger:parameters authorAddRequest
type authorAddRequest struct {
	// Имя автора
	// required:true
	// in:formData
	Firstname string `json:"firstname"`

	// Фамилия автора
	// required:true
	// in:formData
	SecondName string `json:"secondname"`
}

// swagger:route POST /books book bookAddRequest
// Добавление книги
// responses:
//	200: description Added successfuly.

// swagger:parameters bookAddRequest
type bookAddRequest struct {
	// Название книги
	// required:true
	// in:formData
	Title string `json:"title"`

	// ID юзера
	// required:true
	// in:formData
	UserID int `json:"user_id"`

	// ID автора
	// required:true
	// in:formData
	AuthorID int `json:"author_id"`
}

// swagger:route GET /users user getUserRequest
// Список юзеров
// responses:
//	200: getUserResponse

// swagger:parameters getUserRequest
type getUserRequest struct {
	// Список всех пользователей
}

// swagger:response getUserResponse
type getUserResponse struct {
	// in:body
	Bodies []models.User `json:"bodies"`
}

// swagger:route GET /authors author getAuthorRequest
// Список авторов
// responses:
//	200: getAuthorResponse

// swagger:parameters getAuthorRequest
type getAuthorRequest struct {
	// Список всех авторов
}

// swagger:response getAuthorResponse
type getAuthorResponse struct {
	// in:body
	Bodies []models.Author `json:"bodies"`
}

// swagger:route GET /books book getBookRequest
// Список книг
// responses:
//	200: getBookResponse

// swagger:parameters getBookRequest
type getBookRequest struct {
	// Список всех авторов
}

// swagger:response getBookResponse
type getBookResponse struct {
	// in:body
	Bodies []models.Book `json:"bodies"`
}

// swagger:route GET /books/{id} book getBookByIDRequest
// Получение книги по id
// responses:
// 	200: getBookByIDResponse

// swagger:parameters getBookByIDRequest
type getBookByIDRequest struct {
	// ID книги
	// in:path
	ID string `json:"id"`
}

// swagger:response getBookByIDResponse
type getBookByIDResponse struct {
	// in:body
	Body models.Book `json:"body"`
}

// swagger:route POST /books/borrow book borrowBookRequest
// Получение книги
// responses:
// 	200: description: Borrowed successfuly.

// swagger:parameters borrowBookRequest
type borrowBookRequest struct {
	// ID книги
	// in:formData
	BookID int `json:"book_id"`

	// ID пользователя
	// in:formData
	UserID int `json:"user_id"`
}

// swagger:route POST /books/return book returnBookRequest
// Сдача книги
// responses:
// 	200: description: Returned successfuly.

// swagger:parameters returnBookRequest
type returnBookRequest struct {
	// ID книги
	// in:formData
	BookID int `json:"book_id"`
}
